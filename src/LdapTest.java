import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPEntry;
import netscape.ldap.LDAPException;
import netscape.ldap.LDAPSearchResults;

public class LdapTest {

	static LDAPConnection ld = null;
	static final String MY_HOST = "192.168.0.23";
	static final int MY_PORT = 389;
	static {
		ld = new LDAPConnection();
		try {
			ld.connect(MY_HOST, MY_PORT, "baidu\\administrator", "123456");
		} catch (LDAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static List<String> getAllBranch() {
		List<String> branchs = new ArrayList<String>();
		String ENTRYDN = "ou=China,dc=baidu,dc=com";
		String[] attrNames = { "name" };
		LDAPSearchResults res = null;
		LDAPEntry findEntry = null;
		try {
			res = ld.search(ENTRYDN, LDAPConnection.SCOPE_ONE, "objectclass=*",
					attrNames, false);
		} catch (LDAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (res.hasMoreElements()) {
			try {
				findEntry = res.next();
			} catch (LDAPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String temp = findEntry.getAttribute("name").getStringValues()
					.nextElement().toString();
			branchs.add(temp);
		}
		return branchs;
	}

	public static List<String> getDepartments(String branch) {
		List<String> departments = new ArrayList<String>();
		String ENTRYDN = "ou=" + branch + ",ou=China,dc=baidu,dc=com";
		String[] attrNames = { "name" };
		LDAPSearchResults res = null;
		LDAPEntry findEntry = null;
		try {
			res = ld.search(ENTRYDN, LDAPConnection.SCOPE_ONE, "objectclass=*",
					attrNames, false);
		} catch (LDAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (res.hasMoreElements()) {
			try {
				findEntry = res.next();
			} catch (LDAPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String temp = findEntry.getAttribute("name").getStringValues()
					.nextElement().toString();
			departments.add(temp);
		}

		return departments;
	}

	public static List<String> getEmployees(String branch, String department) {
		List<String> employees = new ArrayList<String>();
		String ENTRYDN = "ou=" + department + ",ou=" + branch
				+ ",ou=China,dc=baidu,dc=com";
		String[] attrNames = { "name" };
		LDAPSearchResults res = null;
		LDAPEntry findEntry = null;
		try {
			res = ld.search(ENTRYDN, LDAPConnection.SCOPE_ONE, "objectclass=*",
					attrNames, false);
		} catch (LDAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		while (res.hasMoreElements()) {
			try {
				findEntry = res.next();
			} catch (LDAPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String temp = findEntry.getAttribute("name").getStringValues()
					.nextElement().toString();
			employees.add(temp);
		}
		return employees;
	}

	public static Map<String, String> getEmployee(String sAMAccountName) {
		Map<String, String> emp = new HashMap<String, String>();
		String ENTRYDN = "ou=China,dc=baidu,dc=com";
		String[] attrNames = { "cn"};
		LDAPSearchResults res = null;
		LDAPEntry findEntry = null;
		try {
			res = ld.search(ENTRYDN, LDAPConnection.SCOPE_SUB,
					"sAMAccountName=" + sAMAccountName, attrNames, false);
		} catch (LDAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			findEntry = res.next();
		} catch (LDAPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (int i = 0; i < attrNames.length; i++) {
			emp.put(attrNames[i], findEntry.getAttribute(attrNames[i])
					.getStringValues().nextElement().toString());
		}
		return emp;
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();

		if (ld != null && ld.isConnected()) {
			try {
				ld.disconnect();
			} catch (LDAPException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {

		System.out.println(getEmployee("bjfatest01"));

		System.out.println(getEmployees("beijing", "hr"));

		System.out.println(getDepartments("beijing"));

		System.out.println(getAllBranch());

	}

}