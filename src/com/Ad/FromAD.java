package com.Ad;

import netscape.ldap.LDAPConnection;
import netscape.ldap.LDAPEntry;
import netscape.ldap.LDAPException;
import netscape.ldap.LDAPSearchResults;

public class FromAD { 

	static final String MY_HOST = "192.168.0.23";
	static final int MY_PORT = 389;
        static final String MY_USER_ID = "baidu\\administrator";
        static final String PASSWORD = "123456";
	public static void main(String[] args){
		System.out.println(new FromAD().getList2String("OU=China,DC=baidu,DC=com"));
	}
	public static LDAPConnection getConnection() {
		LDAPConnection conn = new LDAPConnection();  
		try {  
		    conn.connect(MY_HOST, MY_PORT, MY_USER_ID, PASSWORD);  
		} catch (LDAPException e1) {  
		    throw new RuntimeException(e1);  
		}  
		return conn;
	 }
	  public String getList2String( String lname){
		  if(lname == null){
			  lname ="ou=china,dc=baidu,dc=com";
		  }
		  //System.out.println("lname是："+lname);
	   // List child = new ArrayList();
   	 	StringBuilder sb = new StringBuilder("[");
	    try {

	    		LDAPConnection ld = getConnection();
	    		//List<String> branchs = new ArrayList<String>();
	    		String ENTRYDN = lname;
	    		String[] attrNames = { "name","distinguishedName","mail","objectClass" };
	    		LDAPSearchResults res = null;
	    		LDAPEntry findEntry = null;
	    		
	    			res = ld.search(ENTRYDN, LDAPConnection.SCOPE_ONE, "(|(objectclass=organizationalUnit)(objectclass=organizationalPerson))",
	    					attrNames, false);
	    	
	    	if(res.hasMoreElements()){
	    		while (res.hasMoreElements()) {
	    			String stritem = "";
	    			findEntry = res.next();
	    			String temp = findEntry.getAttribute("name").getStringValues()
	    					.nextElement().toString();
	    			String tempdn = findEntry.getAttribute("distinguishedName").getStringValues()
	    					.nextElement().toString();
	    			//branchs.add(temp);
	    			stritem +="{\"name\":\""+temp+"\",";
	    			stritem +="\"lname\":\""+tempdn+"\",";
	    			//tempitem.put("name",temp);
	    			//tempitem.put("lname",tempdn);
	    			String[] tempdns = tempdn.split(",");
	    			if(tempdn.indexOf("CN=") == -1){
		    			if(tempdns.length <=4){
		    				stritem +="\"type\":\"company\",";
		    				stritem +="\"children\":true}";
		    				//tempitem.put("type","company");
		    				//tempitem.put("children",true);
		    			}else{
		    				stritem +="\"type\":\"department\",";
		    				stritem +="\"children\":true}";
		    				//tempitem.put("type","department");
		    				//tempitem.put("children",true);
		    			}
	    			}else{
	    				stritem +="\"type\":\"user\"}";
	    				//tempitem.put("type","user");
	    			}
	    			if(res.hasMoreElements()){
	    				sb.append(stritem+",");
	    			}else{
	    				sb.append(stritem);
	    			}
	    			//child.add(tempitem);
	    		}
	    	  }
	    	sb.append("]");
	    		ld.disconnect();
	   }catch (LDAPException e) {
	    			// TODO Auto-generated catch block
	    			e.printStackTrace();
	   
	    } 
	   //catch(Exception e){
		//	e.printStackTrace();
		//}
	    
	    return sb.toString();
	  }
}